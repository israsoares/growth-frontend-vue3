# Growth-frontend-vue3

## Project Brief

- This project was created used Vue3 + TypeScript | Composition API | TailwindCSS | Vite | Vitest Unit & Integrated Test |Cypress to e2e Tests | Vue-Router.

- This project has architecture folder Atomic Design + View approach, having small components as Atoms, composed Atoms as Molecules, composed Molecules + atoms
  becoming Organisms and View folder with HomePageView, CreateProjectView, EditProjectView.

It's a front end to List|Create|Update Projects using HTTP requests with API connection locally, the frontend is running on http://localhost:5173/ and the
Spring API http://localhost:8090/projects.

- Atomic design ref: <a href="http://demo.patternlab.io/?p=organisms-media-list">Atomic Design Demo</a>

- Atomic design ref: <a href="https://atomicdesign.bradfrost.com/chapter-2/">Atomic By BradFrost Chapters</a>

- Vistest ref: <a href="https://vitest.dev/">

- Vitest guide</a> Vue TestUnit ref: <a href="https://test-utils.vuejs.org/guide">Vue TestUnit Guide</a>

- Vite ref: Vite Guide [a link]("https://vitejs.dev/")

- Tailwind ref: <a href="https://tailwindcss.com/docs/configuration">Tailwind Doc</a>

- Vue 3 composition API ref: <a href="https://vuejs.org/api/composition-api-setup.html">Vue 3 Composition API :green_heart: </a>

## Project Feature

### Total OverView

Project List Page (entry point) User should be able to see filtered total project overview information as:

Nº of projects by Status "Delivered" Nº of projects by Status "New" Nº of projects with Due date in the past, Date due < Today. Most prominent source language
occurence.

### Project Table

In this section user can see a list of projects and its main informations as:

- Id
- Name
- status
- Date Due
- Edit (button edit info)

#### HomePage EntryPoint gif

<img style="float: right;" src="./img/homepage.png">

### Project Actions

#### Filter By Name Status

User can Filter By

- Project Name
- Project Status

<img style="float: right;" src="./img/filterList.gif">

#### SortBy

User can Sort list By

- Id Asc|Desc
- Name Asc|Desc
- Status Asc|Desc
- Date Due Asc|Desc

<img style="float: right;" src="./img/sortbylistproject.gif">

#### Create new Project

User can Create a New Project

- Create Project Button

Navigate on header top by

- Home Link -Create Project Link

<img style="float: right;" src="./img/createProject.gif">

#### Update existentProject

- User can Update a project

<img style="float: right;" src="./img/updateProject.gif">

#### Error 404 and 500

- If try edit a project id that not exist direct by url
- <img style="float: right;" src="./img/error404.gif">

- If API's not communication
- <img style="float: right;" src="./img/error500.gif">

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

```sh
npm run build
npm run test:e2e
```

## ROADMAP

- Create StoryBook to ensure reusable and component library
- Cover 100% unit test components (current 41% components is covered)
- Cover 100% integrated test components
- Mixin to becoming more reusable functions
- Inhance Http statusCode to a file and handler it between components or move response to store(PINIA)
- Create Reusable Component to CustoRadioButtonSelection

## :four_leaf_clover:
