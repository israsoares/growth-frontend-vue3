import { test, describe, expect } from "vitest"
import { mount } from "@vue/test-utils"
import ArrowIcon from "./ArrowIcon.vue"

describe("ArrowIconComponent", () => {
  const wrapper = mount(ArrowIcon, {})
  test("Has svg selector inside component", () => {
    expect(wrapper.find("svg")).toBeTruthy()
  })

  const svg = wrapper.findAll("svg")
  test("Contains just 1 svg selector inside component", () => {
    expect(svg).toBeTruthy()
  })

  test("Not contains more than 1 svg selector inside component", () => {
    expect(svg).toHaveLength(1)
  })
})
