import { test, describe, expect, beforeEach } from "vitest"
import { VueWrapper, mount } from "@vue/test-utils"
import Loading from "./Loading.vue"


let wrapper: VueWrapper<any>
describe("Loading FeedBack component", () => {

  beforeEach(() => {
    wrapper = mount(Loading)
  })
  test("If contains feedback text and it isN't Empty ", () => {
    expect(wrapper.find(("p"))).toBeTruthy()
    expect(wrapper.text()).toBe("Loading ...")
  })
})
