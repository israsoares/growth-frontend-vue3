import { test, describe, expect } from "vitest"
import { mount } from "@vue/test-utils"
import Heading from "./Heading.vue"

describe("Heading component", () => {
  const wrapper = mount(Heading, {
    propsData: {
      headingTitle: "Title H1",
    },
  })
  test("Receiving props...", () => {
    expect(wrapper.props().headingTitle).toBe("Title H1")
  })
  test("Could not empty...", () => {
    expect(wrapper.props().headingTitle).not.empty
  })
})
