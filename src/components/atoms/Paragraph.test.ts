import { test, describe, expect, beforeEach } from "vitest"
import { VueWrapper, mount } from "@vue/test-utils"
import Paragraph from "./Paragraph.vue"

describe("Paragraph component", () => {
  let wrapper: VueWrapper<any>
  beforeEach(() => {
    wrapper = mount(Paragraph, {
      slots: {
        default: [ '<div id="icon">fa-icon</div>' ],
      },
      propsData: {
        textParagraph: "Contain text",
      },
    })
  })
  test("Contains p element and not empty ", () => {
    expect(wrapper.find("p")).toBeTruthy()
    expect(wrapper.text()).not.toBeNull()
  })
  test("Receiving Props", () => {
    expect(wrapper.props().textParagraph).toBe("Contain text")
  })
  test("Paragraph component has slot", () => {
    expect(wrapper.find("#icon").element).toBeTruthy()
  })
})
