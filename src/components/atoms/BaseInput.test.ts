import { test, describe, expect } from "vitest"
import { mount } from "@vue/test-utils"
import BaseInput from "./BaseInput.vue"

describe("BaseInput Component", () => {
  const wrapper = mount(BaseInput, {
    propsData: {
      label: "Label input",
      labelFor: "forInput",
      id: "inputId",
      type: "text",
      modelValue: "['a', 'b', 'c', 'd']",
    },
  })
  test("Receiving success props", () => {
    expect(wrapper.props().label).toBeTruthy()
    expect(wrapper.props().label).toBe("Label input")
    expect(wrapper.props().labelFor).toBeTruthy()
    expect(wrapper.props().labelFor).toBe("forInput")
    expect(wrapper.props().id).toBeTruthy()
    expect(wrapper.props().id).toBe("inputId")
    expect(wrapper.props().type).toBeTruthy()
    expect(wrapper.props().type).toBe("text")
    expect(wrapper.props().modelValue).toBeTruthy()
    expect(wrapper.props().modelValue).toBe("['a', 'b', 'c', 'd']")
  })

  test("Check default input class style", () => {
    expect(wrapper.find("input").attributes("class")).toBe("w-full border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-gray-100")
  })

  test("Emmiter update modelValue", async () => {
    wrapper.vm.$emit("modelValue")
    wrapper.vm.$emit("modelValue", "update")
    // Wait until $emits have been handled
    await wrapper.vm.$nextTick()
    // assert event has been emitted
    expect(wrapper.emitted().modelValue).toBeTruthy()
    expect(wrapper.emitted().modelValue[ 1 ]).toEqual([ "update" ])
  })
})
