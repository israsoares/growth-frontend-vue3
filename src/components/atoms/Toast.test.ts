import { test, describe, expect } from "vitest"
import { mount } from "@vue/test-utils"
import Toast from "./Toast.vue"

describe("Toast success Component", () => {
  const wrapper = mount(Toast, {
    propsData: {
      message: "Showing success",
      typeMessage: "success",
    },
  })
  test("Receiving success props", () => {
    expect(wrapper.props().message).toBeTruthy()
    expect(wrapper.props().message).toBe("Showing success")
    expect(wrapper.props().typeMessage).toBe("success")
  })
  test("Applying success class through props", () => {
    expect(wrapper.classes()).toContain("border-2")
    expect(wrapper.classes()).toContain("border-emerald-600")
  })
})
describe("Toast Error Component", () => {
  const wrapper = mount(Toast, {
    propsData: {
      message: "Showing error message",
      typeMessage: "error",
    },
  })
  test("Receiving error props", () => {
    expect(wrapper.props().message).toBeTruthy()
    expect(wrapper.props().message).toBe("Showing error message")
    expect(wrapper.props().typeMessage).toBe("error")
  })
  test("Applying error class through props", () => {
    expect(wrapper.classes()).toContain("border-2")
    expect(wrapper.classes()).toContain("border-rose-600")
  })
  test("Snapshot TOAST", () => {
    expect(wrapper).toMatchSnapshot()
  })
})
