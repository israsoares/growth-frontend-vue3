import { test, describe, expect } from "vitest";
import { mount } from "@vue/test-utils";
import Button from "./Button.vue";

describe("Base Button", () => {
  const wrapper = mount(Button, {
    slots: {
      default: ['<div id="icon-arrow-up">icon-arrow</div>', '<div id="icon-arrow-down">icon-arrow</div>'],
    },
    propsData: {
      name: "Name",
      buttonName: "Base button name",
    },
  });
  test("Button receiving props", () => {
    //has p
    expect(wrapper.props().name).toBe("Name");
    //has button props
    expect(wrapper.props().buttonName).toBe("Base button name");
  });
  test("Button has slot icon", () => {
    expect(wrapper.find("#icon-arrow-up").exists()).toBe(true);
    expect(wrapper.find("#icon-arrow-down").exists()).toBe(true);
  });
});
