import { test, describe, expect, beforeEach } from "vitest"
import { VueWrapper, mount } from "@vue/test-utils"
import FormProject from "./FormProject.vue"

describe("Form register", () => {
  let wrapper: VueWrapper<any>

  beforeEach(() => {
    wrapper = mount(FormProject, {})
  })
  test("Form mount", () => {
    expect(wrapper).toMatchSnapshot()
  })
})
