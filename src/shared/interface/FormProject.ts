export interface FormProject {
    id: number,
    name: string,
    sourceLanguage: string,
    targetLanguages: string[],
    status: string,
    dateDue: Date | string,
    dateCreated: Date | string,
    dateUpdated: Date | string
}