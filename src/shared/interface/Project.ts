export interface Project {
    "id": number,
    "name": string,
    "sourceLanguage": string,
    "status": string,
    "targetLanguages": string[],
    "dateCreated"?: string,
    "dateUpdated"?: string,
    "dateDue"?: string,
    "_links"?: {
        "self": {
            "href": string
        },
        "project": {
            "href": string
        }
    }
}