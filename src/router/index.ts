import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/HomeView.vue')
    },
    {
      path: '/create-project',
      name: 'create-project',
      // route level code-splitting
      // this generates a separate chunk (CreateView.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('@/views/CreateView.vue')
    },
    {
      path: '/edit',
      name: 'edit',
      children: [
        {
          path: ':id',
          component: (): Promise<any> => import('@/views/EditView.vue'),
          props: route => ({ ...route.params, id: route.params?.id })
        }
      ],
      beforeEnter(to, from, next) {
        if (from.path == "/" && to.params.id)
          next()
        else
          next('/')
      }
    },
    // will match everything and put it under `$route.params.pathMatch`
    {
      path: '/:pathMatch(.*)*',
      name: 'NotFound',
      component: () => import('../components/molecules/RequestError.vue'),
      props: { imageSourcePath: './404.jpg', altText: 'not-found' }
    },
  ]
})

export default router
